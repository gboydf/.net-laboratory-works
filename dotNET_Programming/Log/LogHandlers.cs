﻿using System;
using System.IO;

namespace dotNET_Programming.Log
{
    static class LogHandlers
    {
        /// <summary>
        /// Вывод лога в консоль
        /// </summary>
        /// <param name="args"></param>
        public static void ConsoleLogHandler(LogEventArgs args)
        {
            var defaultConsoleColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine($"{args.Sender.Name}: {args.Message}");
            Console.ForegroundColor = defaultConsoleColor;
        }

        /// <summary>
        /// Запись лога в файл
        /// </summary>
        /// <param name="args"></param>
        public static void FileLogHandler(LogEventArgs args)
        {
            using (var writer = new StreamWriter(args.Config.FileName, true))
            {
                writer.WriteLine($"{args.Sender.Name}: {args.Message}");
            }
        }
    }
}
