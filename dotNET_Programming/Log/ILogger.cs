﻿namespace dotNET_Programming.Log
{
    /// <summary>
    /// Обработчик события логгирования
    /// </summary>
    /// <param name="args">Аргументы события</param>
    delegate void LogEventHandler(LogEventArgs args);

    /// <summary>
    /// Интерфейс логгера
    /// </summary>
    interface ILogger
    {
        /// <summary>
        /// Выполняет логгирование сообщения
        /// </summary>
        /// <param name="message">Заданное сообщение</param>
        void Log(string message);
    }
}
