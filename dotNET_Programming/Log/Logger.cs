﻿using System.Threading;

namespace dotNET_Programming.Log
{
    static class Logger
    {
        private static readonly object locker = new object();

        /// <summary>
        /// Конфигурация логгера.
        /// </summary>
        public static LoggerConfig Config { get; set; } = new LoggerConfig();

        /// <summary>
        /// Событие, которое возникает при логгировании
        /// </summary>
        public static event LogEventHandler OnLog;

        /// <summary>
        /// Вызывает метод OnLog
        /// </summary>
        /// <param name="args"></param>
        public static void Invoke(LogEventArgs args)
        {
            ThreadPool.QueueUserWorkItem((_) =>
            {
                lock (locker)
                {
                    OnLog?.Invoke(args);
                }
            });
        }
    }


    class Logger<T> : ILogger
    {
        public void Log(string message)
        {
            var args = new LogEventArgs(message, typeof(T));
            Logger.Invoke(args);
        }
    }
}
