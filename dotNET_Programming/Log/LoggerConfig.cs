﻿namespace dotNET_Programming.Log
{
    /// <summary>
    /// Конфигурация логгера.
    /// </summary>
    class LoggerConfig
    {
        /// <summary>
        /// Имя файла для записи лога.
        /// </summary>
        public string FileName { get; set; } = "Log.txt";
    }
}
