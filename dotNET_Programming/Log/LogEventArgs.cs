﻿using System;

namespace dotNET_Programming.Log
{
    class LogEventArgs : EventArgs
    {
        /// <summary>
        /// Конфигурация логгера.
        /// </summary>
        public LoggerConfig Config { get; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message
        {
            get;
        }

        /// <summary>
        /// Тип отправитель сообщения.
        /// </summary>
        public Type Sender { get; }

        /// <summary>
        /// Конструктор, создающий новый экземпляр класса LogEventArgs
        /// </summary>
        /// <param name="message">Сообщение</param>
        public LogEventArgs(string message, Type sender)
        {
            Config = Logger.Config;
            Message = message;
            Sender = sender;
        }
    }
}
