﻿using System;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming.Dishes
{
    /// <summary>
    /// Интерфейс блюда
    /// </summary>
    /// <typeparam name="I">Ингредиент</typeparam>
    /// <typeparam name="S">Соус</typeparam>
    interface IDish<out I, out S> : ICloneable where I : IDishIngredient where S : ISauce
    {
        /// <summary>
        /// Основной ингредиент блюда
        /// </summary>
        I DishIngredient
        {
            get;
        }

        /// <summary>
        /// Соус для блюда
        /// </summary>
        S Sauce
        {
            get;
        }

        /// <summary>
        /// Название блюда
        /// </summary>
        string Name
        {
            get;
        }

        /// <summary>
        /// Калорийность блюда
        /// </summary>
        double Calorific
        {
            get;
        }
    }
}
