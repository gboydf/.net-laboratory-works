﻿using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming.Dishes
{
    /// <summary>
    /// Блюдо
    /// </summary>
    class Dish<I, S> : IDish<I, S> where I : IDishIngredient where S : ISauce
    {
        public I DishIngredient
        {
            get;
        }

        public S Sauce
        {
            get;
        }

        public string Name
        {
            get;
        }

        public double Calorific => DishIngredient.Calorific + Sauce.Calorific;

        public Dish(I dishIngredient, S sauce, string name)
        {
            DishIngredient = dishIngredient;
            Sauce = sauce;
            Name = name;
        }

        public object Clone()
        {
            var clone = new Dish<I, S>((I)DishIngredient.Clone(), (S)Sauce.Clone(), Name);
            return clone;
        }

        public override string ToString()
        {
            return $"{Name} has {Calorific} cal.";
        }
    }
}
