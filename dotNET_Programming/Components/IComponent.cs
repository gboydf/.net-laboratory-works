﻿using System;

namespace dotNET_Programming.Components
{
    /// <summary>
    /// Компонент блюда
    /// </summary>
    interface IComponent : ICloneable
    {
        /// <summary>
        /// Состав компонента
        /// </summary>
        string Composition
        {
            get;
        }

        /// <summary>
        /// Калорийность компонента
        /// </summary>
        double Calorific
        {
            get;
        }
    }
}
