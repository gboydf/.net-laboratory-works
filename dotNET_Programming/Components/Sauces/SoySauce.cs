﻿namespace dotNET_Programming.Components.Sauces
{
    /// <summary>
    /// Соевый соус
    /// </summary>
    class SoySauce : ISoySauce
    {
        public string Composition => "Soy sauce";

        public double Calorific => 500;

        public object Clone()
        {
            var clone = new SoySauce();
            return clone;
        }
    }
}
