﻿namespace dotNET_Programming.Components.Sauces
{
    /// <summary>
    /// Сырный соус
    /// </summary>
    class CheeseSauce : ICheeseSauce
    {
        public string Composition => "Cheese sauce";

        public double Calorific => 800;

        public object Clone()
        {
            var clone = new CheeseSauce();
            return clone;
        }
    }
}