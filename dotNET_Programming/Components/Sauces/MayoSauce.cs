﻿namespace dotNET_Programming.Components.Sauces
{
    /// <summary>
    /// Майонезный соус
    /// </summary>
    class MayoSauce : IMayoSauce
    {
        public string Composition => "Mayo sauce";

        public double Calorific => 900;

        public object Clone()
        {
            var clone = new MayoSauce();
            return clone;
        }
    }
}
