﻿namespace dotNET_Programming.Components.Ingredients
{
    /// <summary>
    /// Французский ингредиент - куриная печень
    /// </summary>
    class FrenchIngredient : IDishIngredient
    {
        public string Composition => "Chiken liver";

        public double Calorific => 2500;

        public object Clone()
        {
            var clone = new FrenchIngredient();
            return clone;
        }
    }
}