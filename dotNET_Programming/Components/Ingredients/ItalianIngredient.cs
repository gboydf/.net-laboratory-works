﻿namespace dotNET_Programming.Components.Ingredients
{
    /// <summary>
    /// Итальянский ингредиент - мясо
    /// </summary>
    class ItalianIngredient : IDishIngredient
    {
        public string Composition => "Meat";

        public double Calorific => 2100;

        public object Clone()
        {
            var clone = new ItalianIngredient();
            return clone;
        }
    }
}
