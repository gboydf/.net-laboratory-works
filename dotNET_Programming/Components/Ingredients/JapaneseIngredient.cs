﻿namespace dotNET_Programming.Components.Ingredients
{
    /// <summary>
    /// Японский ингредиент - рыба
    /// </summary>
    class JapaneseIngredient : IDishIngredient
    {
        public string Composition => "Fish";

        public double Calorific => 1500;

        public object Clone()
        {
            var clone = new JapaneseIngredient();
            return clone;
        }
    }
}
