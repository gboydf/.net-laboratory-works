﻿using System;
using dotNET_Programming.Dishes;
using dotNET_Programming.Collection;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming
{
    class Sort
    {
        /// <summary>
        /// Сравнивает блюда по калорийности
        /// В сортировке блюд блюда дут по убыванию калорийности
        /// </summary>
        /// <param name="dishA">Первое блюдо</param>
        /// <param name="dishB">Второе блюдо</param>
        /// <returns>1, если А меньше B, -1, если А больше B, 0, если веса равны</returns>
        private static int CompareDishesByCalorific(IDish<IDishIngredient, ISauce> dishA, IDish<IDishIngredient, ISauce> dishB)
        {
            return dishB.Calorific.CompareTo(dishA.Calorific);
        }

        /// <summary>
        /// Алгоритм пузырьковой сортировки
        /// </summary>
        /// <param name="pack">Набор блюд</param>
        /// <param name="report"></param>
        public static void BubbleSort(IDishPack<IDish<IDishIngredient, ISauce>> pack, Action<int> report)
        {
            for (int i = 0; i < pack.Count; ++i)
            {
                for (int j = 0; j < pack.Count - 1; ++j)
                {
                    if (CompareDishesByCalorific(pack[j], pack[j + 1]) < 0)
                    {
                        var tmp = pack[j];
                        pack[j] = pack[j + 1];
                        pack[j + 1] = tmp;
                    }
                }
                report((int)((double)i / pack.Count * 100));
            }
            report(100);
        }

        /// <summary>
        /// Алгоритм быстрой сортировки
        /// </summary>
        /// <param name="pack"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        private static void QuickSortImpl(IDishPack<IDish<IDishIngredient, ISauce>> pack, int begin, int end)
        {
            var i = begin;
            var j = end;
            var pivot = pack[(i + j) / 2];

            do
            {
                while (CompareDishesByCalorific(pack[i], pivot) < 0) ++i;
                while (CompareDishesByCalorific(pack[j], pivot) > 0) --j;
                if (i <= j)
                {
                    var tmp = pack[i];
                    pack[i] = pack[j];
                    pack[j] = tmp;
                    ++i;
                    --j;
                }
            } while (i <= j);

            if (begin < j)
            {
                QuickSortImpl(pack, begin, j);
            }

            if (i < end)
            {
                QuickSortImpl(pack, i, end);
            }
        }

        public static void QuickSort(IDishPack<IDish<IDishIngredient, ISauce>> pack, Action<int> report)
        {
            if (pack.Count == 0)
            {
                return;
            }
            QuickSortImpl(pack, 0, pack.Count - 1);
        }

    }
}
