﻿using System;

namespace dotNET_Programming.Exceptions
{
    class ConfigurationException : Exception
    {
        public ConfigurationException(string message) : base(message)
        {

        }
    }
}
