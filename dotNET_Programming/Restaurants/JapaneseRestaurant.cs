﻿using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Log;

namespace dotNET_Programming.Restaurants
{
    /// <summary>
    /// Ресторан, который делает блюда японской кухни
    /// </summary>
    class JapaneseRestaurant : IRestaurant
    {
        private static Logger<JapaneseRestaurant> logger = new Logger<JapaneseRestaurant>();

        private static IRestaurant instance = new JapaneseRestaurant();

        public static IRestaurant Instance => instance;

        private JapaneseRestaurant()
        {

        }

        /// <summary>
        /// Японское блюдо с соевым соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ISoySauce> MakeSoyDish()
        {
            var name = "Japanese soy dish";
            var dish = new Dish<JapaneseIngredient, SoySauce>(new JapaneseIngredient(), new SoySauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Японское блюдо с майонезным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, IMayoSauce> MakeMayoDish()
        {
            var name = "Japanese mayo dish";
            var dish = new Dish<JapaneseIngredient, MayoSauce>(new JapaneseIngredient(), new MayoSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Японское блюдо с сырным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ICheeseSauce> MakeCheeseDish()
        {
            var name = "Japanese chese dish";
            var dish = new Dish<JapaneseIngredient, CheeseSauce>(new JapaneseIngredient(), new CheeseSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }
    }
}
