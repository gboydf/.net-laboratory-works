﻿using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming.Restaurants
{
    /// <summary>
    /// Абстракная фабрика по производству различных блюд
    /// </summary>
    interface IRestaurant
    {
        /// <summary>
        /// Создаёт блюдо с соевым соусом
        /// </summary>
        /// <returns></returns>
        IDish<IDishIngredient, ISoySauce> MakeSoyDish();

        /// <summary>
        /// Создаёт блюдо с майонезным соусом
        /// </summary>
        /// <returns></returns>
        IDish<IDishIngredient, IMayoSauce> MakeMayoDish();

        /// <summary>
        /// Создаёт блюдо с сырным соусом
        /// </summary>
        /// <returns></returns>
        IDish<IDishIngredient, ICheeseSauce> MakeCheeseDish();
    }
}
