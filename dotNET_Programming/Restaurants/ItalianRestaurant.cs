﻿using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Log;

namespace dotNET_Programming.Restaurants
{   
    /// <summary>
    /// Ресторан, который делает блюда итальянской кухни
    /// </summary>
    class ItalianRestaurant : IRestaurant
    {
        private static Logger<ItalianRestaurant> logger = new Logger<ItalianRestaurant>();

        private static IRestaurant instance = new ItalianRestaurant();

        public static IRestaurant Instance => instance;

        private ItalianRestaurant()
        {

        }

        /// <summary>
        /// Итальянское блюдо с соевым соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ISoySauce> MakeSoyDish()
        {
            var name = "Italian soy dish";
            var dish = new Dish<ItalianIngredient, SoySauce>(new ItalianIngredient(), new SoySauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Итальянское блюдо с майонезным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, IMayoSauce> MakeMayoDish()
        {
            var name = "Italian mayo dish";
            var dish = new Dish<ItalianIngredient, MayoSauce>(new ItalianIngredient(), new MayoSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Итальянское блюдо с сырным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ICheeseSauce> MakeCheeseDish()
        {
            var name = "Italian cheese dish";
            var dish = new Dish<ItalianIngredient, CheeseSauce>(new ItalianIngredient(), new CheeseSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }
    }
}
