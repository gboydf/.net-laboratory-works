﻿using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Log;

namespace dotNET_Programming.Restaurants
{
    /// <summary>
    /// Ресторан, который делает блюда французской кухни
    /// </summary>
    class FrenchRestaurant : IRestaurant
    {
        private static Logger<FrenchRestaurant> logger = new Logger<FrenchRestaurant>();

        private static IRestaurant instance = new FrenchRestaurant();

        public static IRestaurant Instance => instance;

        private FrenchRestaurant()
        {

        }

        /// <summary>
        /// Французское блюдо с соевым соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ISoySauce> MakeSoyDish()
        {
            var name = "French soy dish";
            var dish = new Dish<FrenchIngredient, SoySauce>(new FrenchIngredient(), new SoySauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Французское блюдо с майонезным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, IMayoSauce> MakeMayoDish()
        {
            var name = "French mayo dish";
            var dish = new Dish<FrenchIngredient, MayoSauce>(new FrenchIngredient(), new MayoSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }

        /// <summary>
        /// Французское блюдо с сырным соусом
        /// </summary>
        /// <returns></returns>
        public IDish<IDishIngredient, ICheeseSauce> MakeCheeseDish()
        {
            var name = "French cheese dish";
            var dish = new Dish<FrenchIngredient, CheeseSauce>(new FrenchIngredient(), new CheeseSauce(), name);
            //logger.Log($"Produced \"{name}\" dish.");
            return dish;
        }
    }
}
