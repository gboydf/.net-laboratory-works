﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using dotNET_Programming.Dishes;
using dotNET_Programming.Collection;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Exceptions;
using dotNET_Programming.Log;
using dotNET_Programming.Restaurants;

namespace dotNET_Programming
{
    /// <summary>
    /// Создаёт набор блюд на вынос, опираясь на файл конфигурации
    /// </summary>
    class DishPackConf
    {
        private static Logger<DishPackConf> logger = new Logger<DishPackConf>();

        private Dictionary<string, int> configuration;

        /// <summary>
        /// Конструктор, создающий новый экземпляр DishPackConf с конфигурацией из файла 'path'
        /// </summary>
        /// <param name="path">Путь к файлу конфигурации</param>
        public DishPackConf(string path)
        {
            configuration = ReadConfig(path);
            logger.Log($"Successfully read configuration from {path}");
        }

        /// <summary>
        /// Производит набор блюд
        /// </summary>
        /// <returns>Набор блюд</returns>
        public DishPack<IDish<IDishIngredient, ISauce>> MakeDishPack()
        {
            var pack = new DishPack<IDish<IDishIngredient, ISauce>>(Sort.BubbleSort);

            var frenchRestaurant = FrenchRestaurant.Instance;
            var italianRestaurant = ItalianRestaurant.Instance;
            var japaneseRestaurant = JapaneseRestaurant.Instance;

            for (int i = 0; i < configuration["frenchSoyDish"]; i++)
            {
                pack.Add(frenchRestaurant.MakeSoyDish());
            }

            for (int i = 0; i < configuration["frenchMayoDish"]; i++)
            {
                pack.Add(frenchRestaurant.MakeMayoDish());
            }

            for (int i = 0; i < configuration["frenchCheeseDish"]; i++)
            {
                pack.Add(frenchRestaurant.MakeCheeseDish());
            }


            for (int i = 0; i < configuration["italianSoyDish"]; i++)
            {
                pack.Add(italianRestaurant.MakeSoyDish());
            }

            for (int i = 0; i < configuration["italianMayoDish"]; i++)
            {
                pack.Add(italianRestaurant.MakeMayoDish());
            }

            for (int i = 0; i < configuration["italianCheeseDish"]; i++)
            {
                pack.Add(italianRestaurant.MakeCheeseDish());
            }


            for (int i = 0; i < configuration["japaneseSoyDish"]; i++)
            {
                pack.Add(japaneseRestaurant.MakeSoyDish());
            }

            for (int i = 0; i < configuration["japaneseMayoDish"]; i++)
            {
                pack.Add(japaneseRestaurant.MakeMayoDish());
            }

            for (int i = 0; i < configuration["japaneseCheeseDish"]; i++)
            {
                pack.Add(japaneseRestaurant.MakeCheeseDish());
            }

            logger.Log("DishPack was produced");

            return pack;
        }

        /// <summary>
        /// Считывает файл конфигурации
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <returns>Конфигурация</returns>
        private Dictionary<string, int> ReadConfig(string path)
        {
            var config = File.ReadAllLines(path)
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Select(s => s.Trim())
                .ToList();

            if (config[0] != "[DishPack]")
            {
                throw new ConfigurationException($"Invalid configuration file format. Section '[DishPack]' was expected");
            }

            var configuration = new Dictionary<string, int>
            {
                { "frenchSoyDish", 0 },
                { "frenchMayoDish", 0},
                { "frenchCheeseDish", 0},
                { "italianSoyDish", 0},
                { "italianMayoDish", 0},
                { "italianCheeseDish", 0},
                { "japaneseSoyDish", 0},
                { "japaneseMayoDish", 0},
                { "japaneseCheeseDish", 0}
            };

            for (int i = 1; i < config.Count; i++)
            {
                var line = config[i].Split(new[] { '=' });

                if (line.Length != 2)
                {
                    throw new ConfigurationException($"Invalid configuration parameter format: '{config[i]}'");
                }

                var parameter = line[0].Trim();
                var parameter_value = line[1].Trim();

                if (!configuration.ContainsKey(parameter))
                {
                    throw new ConfigurationException($"Invalid configuration parameter: '{parameter}'");
                }

                int value = 0;

                if (!int.TryParse(parameter_value, out value))
                {
                    throw new ConfigurationException($"Invalid configuration parameter value: '{parameter_value}'");
                }

                configuration[parameter] = value;
            }

            return configuration;
        }
    }
}
