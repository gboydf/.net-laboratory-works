﻿using System;
using System.Threading;
using dotNET_Programming.Dishes;
using dotNET_Programming.Collection;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Log;
using dotNET_Programming.Exceptions;

namespace dotNET_Programming
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.OnLog += LogHandlers.ConsoleLogHandler;
            var logger = new Logger<Program>();

            try
            {
                var dishpackconf = new DishPackConf("../../Configuration.ini");
                var dishPack = dishpackconf.MakeDishPack();

                var lab5task = dishPack.SortAsync((p) => Console.WriteLine($"Sorting process: {p}%"));

                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Other threads are in progress...");
                    Thread.Sleep(10);
                }

                lab5task.Wait();
            }
            catch (ConfigurationException ex)
            {
                logger.Log($"User defined exception: {ex.GetType().FullName}: {ex.Message}");
            }
            catch (Exception ex)
            {
                logger.Log($"System exception: {ex.GetType().FullName}: {ex.Message}");
            }

            Console.ReadKey();
        }
    }
}
