﻿using System;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;
using dotNET_Programming.Log;

namespace dotNET_Programming.Collection
{
    class DishPack<T> : IDishPack<T> where T : IDish<IDishIngredient, ISauce>
    {
        public static Logger<DishPack<T>> logger = new Logger<DishPack<T>>();

        private readonly List<T> dishes = new List<T>();

        /// <summary>
        /// Индексатор для набора блюд
        /// </summary>
        /// <param name="i">Индекс блюда в наборе</param>
        /// <returns>Блюда с заданнным индексом</returns>
        public T this[int i]
        {
            get
            {
                return dishes[i];
            }
            set
            {
                dishes[i] = value;
            }
        }

        /// <summary>
        /// Количество блюд в наборе
        /// </summary>
        public int Count => dishes.Count;

        public bool IsReadOnly => false;

        /// <summary>
        /// Калорийность набора конфет
        /// </summary>
        public double PackCalorific => dishes.Select(c => c.Calorific).Aggregate((a, b) => a + b);

        /// <summary>
        /// Добавить блюдо в набор
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            dishes.Add(item);
            //logger.Log("New item was added into collection");
        }

        /// <summary>
        /// Очистить набор
        /// </summary>
        public void Clear()
        {
            dishes.Clear();
            //logger.Log("Collection was cleared");
        }

        /// <summary>
        /// Содержит ли набор заданное блюдо
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Содержит или не содержит</returns>
        public bool Contains(T item)
        {
            var contains = dishes.Contains(item);
            return contains;
        }

        public void CopyTo(T[] array, int index)
        {
            dishes.CopyTo(array, index);
        }

        public IEnumerator<T> GetEnumerator()
        {
            var enumerator = new DishPackEnumerator<T>(dishes);
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Удалить элемент из набора
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            //logger.Log("Item was removed from collection");
            return dishes.Remove(item);
        }

        /// <summary>
        /// Применяет функцию "action" для каждого элемента коллекции блюд
        /// </summary>
        /// <param name="action">Функция, которая применяется к элементам коллекции</param>
        public void ForEach(Action<T> action)
        {
            foreach (var dish in dishes)
            {
                action(dish);
            }
        }

        public SortDelegate SortingAlgorithm
        {
            get;
        }

        public DishPack(SortDelegate sortingAlgorithm)
        {
            SortingAlgorithm = sortingAlgorithm;
        }

        public async Task SortAsync(Action<int> report)
        {
            await Task.Run(() => SortingAlgorithm?.Invoke((IDishPack<IDish<IDishIngredient, ISauce>>)this, report));
            logger.Log("Successfully sorted collection");
        }

        /// <summary>
        /// Совершиет сортировку коллекции
        /// </summary>
        public void Sort(Action<int> report)
        {
            SortingAlgorithm?.Invoke((IDishPack<IDish<IDishIngredient, ISauce>>)this, report);
            logger.Log("Collection was sorted");
        }

        /// <summary>
        /// Заменяет элементы коллекции результатами функции
        /// </summary>
        /// <param name="transformer">Функция-трансформер</param>
        public void Transform(DishTransformer<T> transformer)
        {
            for (int i = 0; i < dishes.Count; i++)
            {
                dishes[i] = transformer(dishes[i]);
            }
        }
    }
}
