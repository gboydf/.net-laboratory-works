﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming.Collection
{
    class DishPackEnumerator<T> : IEnumerator<T> where T : IDish<IDishIngredient, ISauce>
    {
        private readonly IList<T> dishes;
        private int current = -1;

        public DishPackEnumerator(IList<T> dishes)
        {
            this.dishes = dishes;
        }

        public T Current => dishes[current];

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            current++;
            return current < dishes.Count;
        }

        public void Reset()
        {
            current = -1;
        }
    }
}
