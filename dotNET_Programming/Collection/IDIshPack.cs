﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using dotNET_Programming.Dishes;
using dotNET_Programming.Components.Ingredients;
using dotNET_Programming.Components.Sauces;

namespace dotNET_Programming.Collection
{
    /// <summary>
    /// Функция, которая меняет конфигурацию блюда (параметры блюда)
    /// </summary>
    /// <typeparam name="T">Тип блюда</typeparam>
    /// <param name="dish">Блюдо</param>
    /// <returns>Блюдо с измененными параметрами</returns>
    delegate T DishTransformer<T>(T dish) where T : IDish<IDishIngredient, ISauce>;

    /// <summary>
    /// Делегат, который реализует алгоритм сортировки заданной коллекции
    /// </summary>
    /// <param name="pack">Коллекция</param>
    delegate void SortDelegate(IDishPack<IDish<IDishIngredient, ISauce>> pack, Action<int> report);

    /// <summary>
    /// Набор из нескольких блюд (на вынос)
    /// </summary>
    /// <typeparam name="T">Тип блюда</typeparam>
    interface IDishPack<T> : ICollection<T> where T : IDish<IDishIngredient, ISauce>
    {
        /// <summary>
        /// Индексатор для набора блюд
        /// </summary>
        /// <param name="i">Индекс блюда</param>
        /// <returns>Блюдо с заданным индексом i</returns>
        T this[int i]
        {
            get;
            set;
        }

        /// <summary>
        /// Общая калорийность набора блюд
        /// </summary>
        double PackCalorific
        {
            get;
        }

        /// <summary>
        /// Функция, которая выполняет сортировку заданной коллекции
        /// </summary>
        SortDelegate SortingAlgorithm
        {
            get;
        }

        /// <summary>
        /// Применяет функцию "action" для каждого элемента коллекции блюд
        /// </summary>
        /// <param name="action">Функция, которая применяется к элементам коллекции</param>
        void ForEach(Action<T> action);

        /// <summary>
        /// Совершиет сортировку коллекции
        /// </summary>
        /// <param name="report">Функция обработки процесса соритровки.</param>
        void Sort(Action<int> report);

        /// <summary>
        /// Выполняет асинхронную сортировку коллекции.
        /// </summary>
        /// <param name="report">Функция обработки процесса соритровки.</param>
        Task SortAsync(Action<int> report);

        /// <summary>
        /// Заменяет элементы коллекции результатами функции
        /// "transformer" к существующим элементам
        /// </summary>
        /// <param name="transformer">Функция-трансформер</param>
        void Transform(DishTransformer<T> transformer);
    }
}
