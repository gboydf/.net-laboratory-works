﻿using dotNET_Programming.Components;

namespace dotNET_Programming.Info
{
    class ComponentInfoProvider : IInfoProvider<IComponent>
    {
        /// <summary>
        /// Возвращает информацию о составляющих объекта
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public string GetInfo(IComponent item)
        {
            var info = "Ingredients: " + item.Composition;
            return info;
        }
    }
}
