﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotNET_Programming.Info
{
    /// <summary>
    /// Даёт доступ к информации
    /// </summary>
    /// <typeparam name="T">Тип, для которого нужно получить информацию</typeparam>
    interface IInfoProvider<in T>
    {
        /// <summary>
        /// Возвращает информацию об объекте
        /// </summary>
        /// <param name="item">Объект, информацию о котором нужно получить</param>
        /// <returns></returns>
        string GetInfo(T item);
    }
}
